import faker from 'faker'
import { Container } from 'native-base'
import { StatusBar } from 'react-native'

import React from 'react'
import { Font, AppLoading, Constants } from 'expo'
import { AppNavigator } from './AppNavigator'
import { configure as configureFirebase } from './config/firebase'
import { AppProvider } from './AppContext'

export default class App extends React.Component {

  state = {
    isReady: false,
  }

  getRandomUser = () => {

    //just randomly create a user, everytime it opened
    return {
      _id: Constants.installationId,
      name: faker.name.findName(),
      avatar: 'https://placeimg.com/100/100/any',
    }
  }

  async componentWillMount () {
    await Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    })

    //configure the firebase
    configureFirebase()

    const user = this.getRandomUser()
    this.setState({ isReady: true, user })
  }

  render () {
    const { isReady, user } = this.state

    if (!isReady) { return <AppLoading/> }

    const context = {
      user
    }

    return (
      <AppProvider value={context}>
        <Container style={{ marginTop: StatusBar.currentHeight }}>
          <AppNavigator/>
        </Container>
      </AppProvider>
    )
  }
}

