import { createAppContainer, createStackNavigator } from 'react-navigation'

import ChatScene from './chat/index'
import ListScene from './list/index'

const navigator = createStackNavigator({
  'listScene': ListScene,
  'chatScene': ChatScene,
}, {
  initialRouteName: 'listScene',
  headerMode: 'none'
})

export const AppNavigator = createAppContainer(navigator)
