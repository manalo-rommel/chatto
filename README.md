# chatto

A simple chat application that uses firebase and query some data from server

#### How to run the code
1. install the dependencies by running `yarn`
2. run the npm script `npm start`
3. this will open up expo environment (on your webbrowser) 
4. using your emulator, you can click the option about opening
the app from emulator


#### Explanation of architecture
I use [expo](https://expo.io/) for faster development experience. 
Also I can build the app even I'm windows machine and targets 
the IOS build and Android build.

I used [native-base](https://nativebase.io/) for styles and 
components, their list uses virtualized so it can handle the 
loading of tons of data efficiently.

I also used the [react-native-gifted-chat](https://github.com/FaridSafi/react-native-gifted-chat) 
which a cool component for chats, the common components for chat 
are available here.

Common libraries like [react-navigation](https://reactnavigation.org/docs/en/getting-started.html) for app navigation, 
[momentjs](https://momentjs.com/) for date manipulation, and 
[lodash](https://lodash.com/) as a javascript utilities for objects and arrays

React 16.5.0 was used, so I can keep away from Redux hellish experience, React Context is fine for passing states between components

Firebase was used and setup using my own Google account. It was a good choice
because of realtime database which propagate changes to those who subscribe to it  


#### If you had more time, what would you like to improve?
1. User management, the chat looks weired because everytime I open up the app
a new userid and avatar are used on my phone
2. Code clean up for re-usability and testability
