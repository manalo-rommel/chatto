import React from 'react'
import { Button, Icon } from 'native-base'
import { withNavigation } from 'react-navigation'

let ArrowBackButton = ({navigation}) => {
  return (
    <Button transparent onPress={() => navigation.goBack()}>
      <Icon name="arrow-back"/>
    </Button>
  )
}

ArrowBackButton = withNavigation(ArrowBackButton)
export { ArrowBackButton }
