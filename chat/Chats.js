import React from 'react'
import { View, Platform } from 'react-native'
import { GiftedChat } from 'react-native-gifted-chat'
import KeyboardSpacer from 'react-native-keyboard-spacer'
import * as firebase from 'firebase'
import moment from 'moment'
import _ from 'lodash'

export class Chats extends React.Component {

  state = {
    messages: [],
    user: null,
  }

  storeChat = (movieId, chat) => {
    firebase.database().ref('chats/' + movieId).push(chat)
  }

  listenToChats = () => {
    const { movieId } = this.props

    firebase.database().ref('chats/' + movieId).on('child_added', (snapshot) => {
      const message = snapshot.val()

      //store only here if the userId is not me
      const { user } = this.props
      if (message.user._id === user._id) return

      this.appendChat(message)

    })
  }

  getInitialChats = () => {
    const { movieId } = this.props

    return firebase.database().ref('/chats/' + movieId).once('value')
      .then((snapshot) => {

        let messages = []
        snapshot.forEach(m => {
          messages.push(m.val())
        })

        //re-sort the message
        messages = _.orderBy(messages, ['createdAt'], ['desc'])

        this.setState({ messages })
      })
  }

  onSend = (messages = []) => {

    const { movieId } = this.props
    let message = {
      ...messages[0],
      createdAt: moment(messages[0].createdAt).toISOString(),
    }

    this.appendChat(message)

    this.storeChat(movieId, message)

  }

  appendChat = (messages) => {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))
  }

  componentWillMount () {

    //retrieved from the database for all chats for this movieId
    this.getInitialChats()

  }

  componentDidMount () {

    //get from firebase
    this.listenToChats()
  }

  render () {

    //every time you open this screen, you are really a random user
    const { user } = this.props
    const { messages } = this.state

    return (
      <View style={{ flex: 1 }}>
        <GiftedChat
          messages={messages}
          onSend={m => this.onSend(m)}
          user={user}
        />
        {Platform.OS === 'android' ? <KeyboardSpacer/> : null}
      </View>

    )
  }
}


