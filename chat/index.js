import { Body, Container, Header, Title, Left, Content, } from 'native-base'
import React from 'react'
import { ArrowBackButton } from './ArrowBackButton'
import { Chats } from './Chats'

import { AppConsumer } from '../AppContext'

export default class ChatScene extends React.Component {
  render () {
    const { navigation } = this.props
    const title = navigation.getParam('title', '')
    const rank = navigation.getParam('rank', '')

    //use this values to determine the id of the movie, because the movies.json doesn't result any id
    const movieId = `${rank}-${title}`

    return (
      <AppConsumer>
        {
          ({ user }) => (<Container style={{ flex: 1 }}>
            <Header>
              <Left>
                <ArrowBackButton/>
              </Left>
              <Body>
              <Title>{title}</Title>
              </Body>
            </Header>

            <Chats  {...{ movieId, user }}/>

          </Container>)
        }
      </AppConsumer>
    )
  }
}
