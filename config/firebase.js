import * as firebase from 'firebase'

export const configure = () => {
// Initialize Firebase
  const firebaseConfig = {
    apiKey: 'AIzaSyDVUx7RWHXXwi2aUwLzY-NMNOYA1_uogu8',
    authDomain: 'chatto-app.firebaseapp.com',
    databaseURL: 'https://chatto-app.firebaseio.com/',
    storageBucket: 'chatto-app.appspot.com'
  }

  firebase.initializeApp(firebaseConfig)
}
