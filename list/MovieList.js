import React from 'react'
import { Text, List, ListItem, Right, Left, Icon } from 'native-base'

const renderRow = ({ title, rank }, navigation) => {
  return (
    <ListItem onPress={() => navigation.navigate('chatScene', { title, rank })}>
      <Left>
        <Text>{title}</Text>
      </Left>
      <Right>
        <Icon name="arrow-forward"/>
      </Right>
    </ListItem>
  )
}

export const MovieList = ({ movies, navigation }) => {

  return (
    <List dataArray={movies} renderRow={(movie) => renderRow(movie, navigation)}/>
  )
}





