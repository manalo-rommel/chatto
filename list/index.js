import { Container, Header, Body, Title } from 'native-base'
import React from 'react'

import { MovieList } from './MovieList'

export default class ChatScene extends React.Component {

  state = {
    movies: []
  }

  async componentDidMount () {
    //get the movies online
    //no paging so just load them all one-shot

    const url = 'https://tender-mclean-00a2bd.netlify.com/mobile/movies.json'
    const response = await fetch(url)
    //just return for now,
    if (response.status >= 400) return

    //set the retrieved movies to state
    const movies = await response.json()
    this.setState({ movies })

  }

  render () {

    const { navigation } = this.props
    const { movies } = this.state

    return (
      <Container>
        <Header>
          <Body>
          <Title>Movies</Title>
          </Body>
        </Header>
        <MovieList {...{ movies, navigation }}  />
      </Container>
    )
  }
}
